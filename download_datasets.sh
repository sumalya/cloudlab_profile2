cd /home/playground/zip
# Download Ad analytics
kaggle datasets download -d shubhamsumalya/wordcount
unzip wordcount.zip
tar -xf word-count.tar.xz -C /home/playground/

# Download Click analytics
kaggle datasets download -d shubhamsumalya/sentimentanalysis
unzip sentimentanalysis.zip
tar -xf sentimentAnalysis_tweetstream.tar.xz -C /home/playground/

# Download Google cloud monitoring
kaggle datasets download -d shubhamsumalya/spikedetection
unzip spikedetection.zip
tar -xf spikeDetection_sensors.tar.xz -C /home/playground/

# Download Google cloud monitoring
kaggle datasets download -d shubhamsumalya/bargainindex
unzip bargainindex.zip
tar -xf stocks.tar.xz -C /home/playground/

# Download Click analytics
kaggle datasets download -d shubhamsumalya/machineoutlier
unzip machineoutlier.zip
tar -xf outlier.tar.xz -C /home/playground/

# Download Click analytics
kaggle datasets download -d shubhamsumalya/trafficmonitoring
unzip trafficmonitoring.zip
tar -xf taxi-traces.tar.xz -C /home/playground/

# Download Click analytics
kaggle datasets download -d shubhamsumalya/clickstream
unzip clickstream.zip
tar -xf click-stream.tar.xz -C /home/playground/

# Download Click analytics
kaggle datasets download -d shubhamsumalya/smartgrid
unzip smartgrid.zip
tar -xf smart-grid.tar.xz -C /home/playground/

# Download Click analytics
kaggle datasets download -d shubhamsumalya/tpchdataset
unzip tpchdataset.zip
tar -xf tpc_h.tar.xz -C /home/playground/

# Download Click analytics
kaggle datasets download -d shubhamsumalya/adanalytics
unzip adanalytics.zip
tar -xf adAnalytics.tar.xz -C /home/playground/

# Download Click analytics
kaggle datasets download -d shubhamsumalya/googlecloudmonitoring
unzip googlecloudmonitoring.zip
tar -xf googleCloudMonitoring.tar.xz -C /home/playground/

# Download Click analytics
kaggle datasets download -d shubhamsumalya/httplogprocessing
unzip httplogprocessing.zip
tar -xf http.tar.xz -C /home/playground/

# Download Click analytics
kaggle datasets download -d shubhamsumalya/linearroad
unzip linearroad.zip
tar -xf lrb.tar.xz -C /home/playground/

# Download Click analytics
kaggle datasets download -d shubhamsumalya/trendingtopics
unzip trendingtopics.zip
tar -xf ttopic.tar.xz -C /home/playground/



